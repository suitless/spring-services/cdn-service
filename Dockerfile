FROM openjdk:11
COPY /build/libs/nl.suitless.cdn-service-0.0.1-SNAPSHOT.jar run.jar
EXPOSE 3305
CMD "java" "-jar" "run.jar"
