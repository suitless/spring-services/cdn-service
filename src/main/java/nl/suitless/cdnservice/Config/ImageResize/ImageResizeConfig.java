package nl.suitless.cdnservice.Config.ImageResize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ImageResizeProperties.class)
public class ImageResizeConfig {
    private final ImageResizeProperties imageResizeProperties;

    @Autowired
    public ImageResizeConfig(ImageResizeProperties imageResizeProperties) {
        this.imageResizeProperties = imageResizeProperties;
    }

    public int getHeight() { return this.imageResizeProperties.getHeight(); }
    public int getWidth() { return this.imageResizeProperties.getWidth(); }
}
