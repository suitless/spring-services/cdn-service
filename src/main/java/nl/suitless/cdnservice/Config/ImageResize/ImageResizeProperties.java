package nl.suitless.cdnservice.Config.ImageResize;

import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotBlank;

@ConfigurationProperties(prefix = "image-resize")
public class ImageResizeProperties {
    @NotBlank(message = "The image resize height must be given in properties")
    private int height;

    @NotBlank(message =  "The image resize width must be given in properties")
    private int width;

    public ImageResizeProperties() {
    }

    @NotBlank
    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @NotBlank
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
