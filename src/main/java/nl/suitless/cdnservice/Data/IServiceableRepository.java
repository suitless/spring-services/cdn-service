package nl.suitless.cdnservice.Data;

import nl.suitless.cdnservice.Domain.Entities.Serviceable;
import nl.suitless.cdnservice.Domain.Entities.ServiceableMeta;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IServiceableRepository extends CrudRepository<Serviceable, Integer> {
    Page<Serviceable> findAllByOwnerAndTypeContaining(String owner, String type, Pageable pageable);
    Optional<Serviceable> findByTagAndOwner(String tag, String owner);
    Optional<Serviceable> findDistinctFirstById(String id);
    Optional<ServiceableMeta> findFirstById(String id);
    List<Serviceable> findAllByTypeAndOwner(String type, String owner);

    Page<ServiceableMeta> getAllByDataIsNotNullAndOwnerAndTypeContainingAndTagContainingOrderByTag(String owner, String type, String tag, Pageable pageable);
    Page<ServiceableMeta> getAllByDataIsNotNullAndOwnerAndTypeContainingOrderByTag(String owner, String type, Pageable pageable);
    Page<ServiceableMeta> getAllByDataIsNotNullAndOwnerAndTypeContainingOrderBySize(String owner, String type, Pageable pageable);

    @Modifying
    @Query(value = "FROM serviceable SELECT distinct type", nativeQuery = true)
    List<String> findAllContentTypes();
}
