package nl.suitless.cdnservice.Services.Utils;

import nl.suitless.cdnservice.Domain.Entities.Serviceable;
import nl.suitless.cdnservice.Domain.Entities.ServiceableMeta;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

public final class PageListTranslator {
    private PageListTranslator() {}

    public static List<Serviceable> PageToList(Page<Serviceable> page) {
        if(page.hasContent()) {
            return new ArrayList<>(page.getContent());
        } else {
            return new ArrayList<>();
        }
    }

    public static List<ServiceableMeta> PageMetaToList(Page<ServiceableMeta> page) {
        if(page.hasContent()) {
            return new ArrayList<>(page.getContent());
        } else {
            return new ArrayList<>();
        }
    }
}
