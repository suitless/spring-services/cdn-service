package nl.suitless.cdnservice.Services.Serviceable;

import nl.suitless.cdnservice.Config.ImageResize.ImageResizeConfig;
import nl.suitless.cdnservice.Data.IServiceableRepository;
import nl.suitless.cdnservice.Domain.Entities.*;
import nl.suitless.cdnservice.Domain.Exceptions.*;
import nl.suitless.cdnservice.Services.Authorization.IAuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import java.util.Base64;
import java.awt.Graphics2D;

@Component
public class ServiceableService implements IServiceableService {
    private IServiceableRepository serviceableRepository;
    private IAuthorizationService authService;
    private ImageResizeConfig imageResizeConfig;

    @Autowired
    public ServiceableService(IServiceableRepository serviceableRepository, IAuthorizationService authService, ImageResizeConfig imageResizeConfig) {
        this.serviceableRepository = serviceableRepository;
        this.authService = authService;
        this.imageResizeConfig = imageResizeConfig;
    }

    @Override
    public List<String> getAllServiceableTypes() {
        return serviceableRepository.findAllContentTypes();
    }

    @Override
    public Serviceable getServiceable(String tag, String owner){
        return serviceableRepository.findByTagAndOwner(tag, owner).orElseThrow(() ->
                new ServiceableNotFoundException("No serviceable exists with tag: " + tag));
    }

    @Override
    public Serviceable getImageByIdSmall(String id) {
        Serviceable foundServiceable = getServiceableByIdIfPresent(id);
        String type = foundServiceable.getType().toLowerCase();
        if (type.contains("image") && (type.contains("jpg") || type.contains("png"))){
            try {
                BufferedImage image = ImageIO.read(foundServiceable.getData().getBinaryStream());

                BufferedImage smallImage = resizeImage(image, imageResizeConfig.getWidth(), imageResizeConfig.getHeight());

                // convert the small image back to blob/ byte[] and return it
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                ImageIO.write(smallImage, foundServiceable.getType().replace("image/", ""), os);
                foundServiceable.setData(new SerialBlob(os.toByteArray()));
                return foundServiceable;
            } catch (IOException | SQLException e) {
                e.printStackTrace();
                throw new MetaDataException("Something went wrong when resizing, only jpg and png are supported.");
            }
        } else {
            throw new ServiceableNotSupportedException("This is not an image");
        }
    }

    BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) throws IOException {
        BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = resizedImage.createGraphics();
        graphics2D.drawImage(originalImage, 0, 0, targetWidth, targetHeight, null);
        graphics2D.dispose();
        return resizedImage;
    }

    @Override
    public Serviceable getServiceableById(String id) {
        return getServiceableByIdIfPresent(id);
    }

    @Override
    public Serviceable createServiceable(String tag, String type, MultipartFile file, boolean locked, String owner, AuthorizationConfig authConfig) {
        try {
            Blob blob = new SerialBlob(StreamUtils.copyToByteArray(file.getInputStream()));
            return genericCreateServiceable(tag, type, blob, locked, owner, authConfig);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }

        throw new ServiceableNotCreatedException("an error occurred.");
    }

    @Override
    public Serviceable createServiceable(String tag, String type, String file, boolean locked, String owner, AuthorizationConfig authConfig) {
        try {
            Blob blob = new SerialBlob(Base64.getDecoder().decode(file));
            return genericCreateServiceable(tag, type, blob, locked, owner, authConfig);
        } catch (SQLException | IllegalArgumentException e) {
            e.printStackTrace();
        }

        throw new ServiceableNotCreatedException("an error occurred.");
    }

    private Serviceable genericCreateServiceable(String tag, String type, Blob blob, boolean locked, String owner, AuthorizationConfig authConfig) {
        authService.check(authConfig.getAuthorities());
        authService.checkUserInBusiness(authConfig.getJwtToken(), owner);

        tag = checkTagAlreadyExists(tag, owner);

        Serviceable serviceable = null;
        try {
            serviceable = new Serviceable(blob, tag, type, blob.length(), locked, owner);
            return serviceableRepository.save(serviceable);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        throw new ServiceableNotCreatedException("an error occurred.");
    }

    @Override
    public Serviceable updateServiceable(String id, String tag, String type, MultipartFile file, boolean locked, String owner, AuthorizationConfig authConfig) {
        try {
            Blob blob = new SerialBlob(StreamUtils.copyToByteArray(file.getInputStream()));
            return genericUpdateServiceable(id, tag, type, blob, locked, owner, authConfig);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }

        throw new ServiceableNotCreatedException("an error occurred.");
    }

    @Override
    public Serviceable updateServiceable(String id, String tag, String type, String file, boolean locked, String owner, AuthorizationConfig authConfig) {
        try {
            Blob blob = new SerialBlob(Base64.getDecoder().decode(file));
            return genericUpdateServiceable(id, tag, type, blob, locked, owner, authConfig);
        } catch (SQLException | IllegalArgumentException e){
            e.printStackTrace();
        }

        throw new ServiceableNotCreatedException("an error occurred.");
    }

    //#region generic update method

    private Serviceable genericUpdateServiceable(String id, String tag, String type, Blob blob, boolean locked, String owner, AuthorizationConfig authConfig) {
        authService.check(authConfig.getAuthorities());
        authService.checkUserInBusiness(authConfig.getJwtToken(), owner);

        try {
            Serviceable serviceable = getServiceableByIdIfPresent(id);
            serviceable.setType(type);
            serviceable.setLocked(locked);
            serviceable.setData(blob);
            serviceable.setSize(blob.length());
            tag = checkTagAlreadyExists(tag, owner);
            serviceable.setTag(tag);
            return serviceableRepository.save(serviceable);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        throw new ServiceableNotCreatedException("an error occurred.");
    }

    //#endregion

    @Override
    public void deleteServiceable(String id, AuthorizationConfig authConfig) {
        Serviceable foundServiceable = getServiceableByIdIfPresent(id);

        authService.check(authConfig.getAuthorities());
        authService.checkUserInBusiness(authConfig.getJwtToken(), foundServiceable.getOwner());

        if(!foundServiceable.getLocked()){
            serviceableRepository.delete(foundServiceable);
        } else {
            throw new ServiceableDefaultException(
                    "Serviceable with id: " + foundServiceable.getId() + " and with tag: " + foundServiceable.getTag() + " is set Locked and can't be deleted");
        }
    }

    @Override
    public Page<ServiceableMeta> getAllMetaData(ServiceableType serviceableType, SortingType sortType,
                                                String owner, int page, int size, AuthorizationConfig authConfig) {
        authService.check(authConfig.getAuthorities());
        authService.checkUserInBusiness(authConfig.getJwtToken(), owner);

        Pageable paging = PageRequest.of(page, size);
        switch (sortType) {
            case TAG:
                return serviceableRepository.getAllByDataIsNotNullAndOwnerAndTypeContainingOrderByTag(owner, serviceableType.getType(), paging);
            case SIZE:
                return serviceableRepository.getAllByDataIsNotNullAndOwnerAndTypeContainingOrderBySize(owner, serviceableType.getType(), paging);
        }

        return serviceableRepository.getAllByDataIsNotNullAndOwnerAndTypeContainingOrderByTag(owner, serviceableType.getType(), paging);
    }

    @Override
    public Page<ServiceableMeta> searchMetaDataByTag(ServiceableType serviceableType, String owner, String tag,
                                                     int page, int size, AuthorizationConfig authConfig) {
        authService.check(authConfig.getAuthorities());
        authService.checkUserInBusiness(authConfig.getJwtToken(), owner);

        Pageable paging = PageRequest.of(page, size);
        return serviceableRepository.getAllByDataIsNotNullAndOwnerAndTypeContainingAndTagContainingOrderByTag(
                owner, serviceableType.getType(), tag, paging);
    }

    @Override
    public ServiceableMeta getMetaData(String id) {
        return serviceableRepository.findFirstById(id).orElseThrow(() ->
                new ServiceableNotFoundException("No serviceable fount with id: " + id));
    }

    @Override
    public ServiceableMeta updateMetaData(String id, String tag, boolean locked, String owner, AuthorizationConfig authConfig) {
        authService.checkAdminOnly(authConfig.getAuthorities());

        Serviceable s = getServiceableById(id);
        s.setLocked(locked);
        s.setOwner(owner);
        tag = checkTagAlreadyExists(tag, owner);
        s.setTag(tag);

        serviceableRepository.save(s);

        try{
            return new ServiceableMeta(s.getId(), s.getTag(), s.getType(), s.getData().length(), s.getLocked());
        } catch (SQLException e) {
            throw new MetaDataException(e.getMessage());
        }
    }

    //#region Helper methods

    private Serviceable getServiceableByIdIfPresent(String id) {
        return serviceableRepository.findDistinctFirstById(id).orElseThrow(() ->
                new ServiceableNotFoundException("No serviceable exists with id: " + id));
    }

    //TODO: this can be a bit more database efficient, now you call the database more than ones....
    private String checkTagAlreadyExists(String tag, String owner){
        tag = tag.replace(' ', '_');
        //check if the content with the given tag already exists
        if(serviceableRepository.findByTagAndOwner(tag, owner).isPresent()){
            //the tag already exists so we have to change the name a bit.
            //we will ad a (x) at the end of the tag, x will be a number that auto increments
            int number = 1;
            while (serviceableRepository.findByTagAndOwner(tag + " " + number, owner).isPresent()){
                number++;
            }
            //found a number that isn't used yet
            tag += "_" + number;
            return tag;
        }

        return tag;
    }

    //#endregion
}
