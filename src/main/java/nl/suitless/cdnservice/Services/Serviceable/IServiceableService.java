package nl.suitless.cdnservice.Services.Serviceable;

import nl.suitless.cdnservice.Domain.Entities.*;
import nl.suitless.cdnservice.Domain.Exceptions.*;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IServiceableService {
    /**
     * The CDN can contain an unlimited type of files, this lists all available types.
     * Only allowed for admins.
     * @return a list of all serviceableTypes
     */
    List<String> getAllServiceableTypes();

    /**
     * Find serviceable by tag.
     * @param tag serviceable tag
     * @param owner of the serviceable you want to find
     * @return serviceable with given tag
     * @throws ServiceableNotFoundException if serviceable with given tag is not found in the system
     */
    Serviceable getServiceable(String tag, String owner);

    /**
     * Find serviceable (image format only, e.g. png) by id and get a small version.
     * @param id serviceable id without dashes
     * @return serviceable with given id
     * @throws ServiceableNotFoundException if serviceable with given id is not found in the system
     * @throws ServiceableNotSupportedException if the serviceable is not an image
     * @throws MetaDataException if the image resizing failed
     */
    Serviceable getImageByIdSmall(String id);

    /**
     * Find serviceable by id.
     * @param id serviceable id without dashes
     * @return serviceable with given id
     * @throws ServiceableNotFoundException if serviceable with given id is not found in the system
     */
    Serviceable getServiceableById(String id);

    /**
     * Creates and uploads serviceable to the database.
     * It will also check if the user is in the correct business and authorized.
     * @param tag unique for serviceable
     * @param type for serviceable
     * @param file stream of serviceable
     * @param locked set the serviceable default state (this prevents deletion)
     * @param owner of the serviceable
     * @param authConfig configuration to check the authorizations
     * @return uploaded serviceable
     * @throws ServiceableNotCreatedException if an error occurred when pushing a serviceable to the database
     */
    Serviceable createServiceable(String tag, String type, MultipartFile file, boolean locked, String owner, AuthorizationConfig authConfig);

    /**
     * Creates and uploads serviceable to the database.
     * It will also check if the user is in the correct business and authorized.
     * @param tag unique for serviceable
     * @param type for serviceable
     * @param file base64 of serviceable
     * @param locked sets the serviceable default state (this prevents deletion)
     * @param owner of the serviceable
     * @param authConfig configuration to check the authorizations
     * @return uploaded serviceable
     * @throws ServiceableNotCreatedException if an error occurred when pushing a serviceable to the database
     */
    Serviceable createServiceable(String tag, String type, String file, boolean locked, String owner, AuthorizationConfig authConfig);

    /**
     * Updates a serviceable.
     * It will also check if the user is in the correct business and authorized.
     * @param id of the serviceable to update
     * @param tag of the serviceable to update
     * @param type type of the serviceable to update
     * @param file stream of the serviceable to update
     * @param locked sets the serviceable default state (this prevents deletion)
     * @param owner of the serviceable
     * @param authConfig configuration to check the authorizations
     * @return updated serviceable
     * @throws ServiceableNotFoundException if serviceable with given tag is not found in the system
     * @throws ServiceableNotCreatedException if an error occurred when pushing a serviceable to the database
     */
    Serviceable updateServiceable(String id, String tag, String type, MultipartFile file, boolean locked, String owner, AuthorizationConfig authConfig);

    /**
     * Updates a serviceable.
     * It will also check if the user is in the correct business and authorized.
     * @param id of the serviceable to update
     * @param tag of the serviceable to update
     * @param type type of the serviceable to update
     * @param file base64 of the serviceable to update
     * @param locked sets the serviceable default state (this prevents deletion)
     * @param owner of the serviceable
     * @param authConfig configuration to check the authorizations
     * @return updated serviceable
     * @throws ServiceableNotFoundException if serviceable with given tag is not found in the system
     * @throws ServiceableNotCreatedException if an error occurred when pushing a serviceable to the database
     */
    Serviceable updateServiceable(String id, String tag, String type, String file, boolean locked, String owner, AuthorizationConfig authConfig);

    /**
     * Deletes serviceable by id.
     * It will also check if the user is in the correct business and authorized.
     * @param id of serviceable
     * @param authConfig configuration to check the authorizations
     * @throws ServiceableNotFoundException if serviceable with given id is not found in the system
     * @throws ServiceableDefaultException if serviceable with give tag is default and cannot be deleted from the system
     */
    void deleteServiceable(String id, AuthorizationConfig authConfig);

    /**
     * Creates a list filled with the metadata wrappers for all serviceable.
     * It will also check if the user is in the correct business and authorized.
     * @param serviceableType of the serviceable you want to obtain
     * @param sortType how you want to sort the list of meta data
     * @param owner of the meta data you want to obtain
     * @param page pagination page you want to retrieve
     * @param size of the page
     * @param authConfig configuration to check the authorizations
     * @return list of metadata wrappers
     */
    Page<ServiceableMeta> getAllMetaData(ServiceableType serviceableType, SortingType sortType,
                                                  String owner, int page, int size, AuthorizationConfig authConfig);

    /**
     * Search for meta data containing the given tag
     * @param serviceableType of the serviceable you want to obtain
     * @param owner of the meta data you want to obtain
     * @param tag you want to search for
     * @param page pagination page you want to retrieve
     * @param size of the page
     * @param authConfig configuration to check the authorizations
     * @return list of metadata wrappers
     */
    Page<ServiceableMeta> searchMetaDataByTag(ServiceableType serviceableType, String owner, String tag,
                                              int page, int size, AuthorizationConfig authConfig);

    /**
     * Find serviceable by id and return all its metadata.
     * @param id serviceable id without dashes
     * @return metadata wrapper
     * @throws ServiceableNotFoundException if serviceable with given id is not found in the system
     * @throws MetaDataException if an error occurred when getting the metadata of a serviceable
     */
    ServiceableMeta getMetaData(String id);

    /**
     * Update the meta data of a serviceable.
     * Only allowed for admins.
     * @param id of the serviceable to update
     * @param tag of the serviceable to update
     * @param locked default state of the serviceable to update
     * @param owner of the serviceable to update
     * @param authConfig configuration to check the authorizations
     * @return updated serviceable
     * @throws ServiceableNotFoundException if serviceable with given tag is not found in the system
     * @throws MetaDataException if an error occurred when getting the metadata of a serviceable
     */
    ServiceableMeta updateMetaData(String id, String tag, boolean locked, String owner, AuthorizationConfig authConfig);
}
