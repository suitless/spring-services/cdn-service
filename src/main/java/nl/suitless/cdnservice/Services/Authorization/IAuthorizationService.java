package nl.suitless.cdnservice.Services.Authorization;

import nl.suitless.cdnservice.Domain.Exceptions.UnauthorizedException;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public interface IAuthorizationService {
    /**
     * Check if the user has the correct authorization to get certain data.
     * Look in the application.yml under authorization.roles for the allowed roles.
     * @param authorities is a collection of granted authorities in the form of a String
     * @throws UnauthorizedException if the user is not authorised
     */
    void check(Collection<GrantedAuthority> authorities);

    /**
     * Check if the user is has admin authorization to get certain data.
     * Look in the application.yml under authorization.admin-role for the allowed role.
     * @param authorities is a collection of granted authorities in the form of a String
     * @throws UnauthorizedException if the user is not authorised
     */
    void checkAdminOnly(Collection<GrantedAuthority> authorities);

    /**
     * Check if the user is in the correct business.
     * If the business doesn't exist it will also return unauthorized.
     * @param jwtToken of the user needed to get the user authorities on the profile-service
     * @param businessName of the business to check
     * @throws UnauthorizedException if the user is not authorised
     */
    void checkUserInBusiness(String jwtToken, String businessName);
}
