package nl.suitless.cdnservice.Domain.Entities;

/**
 * These are all the types that the CDN expects to be in there.
 * It is possible to store other types but retrieving some of the information is not supported.
 */
public enum ServiceableType {
    IMAGE("image"),
    PDF("pdf"),
    ALL("");

    private final String type;
    ServiceableType(final String type) { this.type = type; }

    public String getType() {
        return type;
    }
}
