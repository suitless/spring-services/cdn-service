package nl.suitless.cdnservice.Domain.Entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Blob;

@Entity
public class Serviceable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Lob
    private Blob data;
    @NotNull
    @Column(unique = true)
    private String tag;
    @NotNull
    private String type;
    @NotNull
    private long size;
    @NotNull
    @Column(columnDefinition = "boolean NOT NULL DEFAULT 0")
    private Boolean locked;

    //Ownership property
    @NotNull
    private String owner;

    public Serviceable() {
        /*Thank you, JPA*/
    }

    public Serviceable(@NotNull Blob data, @NotNull String tag, @NotNull String type, @NotNull long size,
                       @NotNull boolean locked, @NotNull String owner) {
        this.data = data;
        this.tag = tag;
        this.type = type;
        this.size = size;
        this.locked = locked;
        this.owner = owner;
    }

    public String getId() {
        return id;
    }

    public Blob getData() {
        return data;
    }

    public void setData(Blob data) {
        this.data = data;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
