package nl.suitless.cdnservice.Domain.Entities;

public enum SortingType {
    TAG,
    SIZE;
}
