package nl.suitless.cdnservice.Domain.Entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ServiceableMeta {
    @Id
    private String id;
    private String tag;
    private String type;
    private long size;
    private boolean locked;

    public ServiceableMeta(String id, String tag, String type, long size, boolean locked) {
        this.id = id;
        this.tag = tag;
        this.type = type;
        this.size = size;
        this.locked = locked;
    }

    public ServiceableMeta(Serviceable serviceable) {
        this.id = serviceable.getId();
        this.tag = serviceable.getTag();
        this.type = serviceable.getType();
        this.size = serviceable.getSize();
        this.locked = serviceable.getLocked();
    }

    public ServiceableMeta() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}
