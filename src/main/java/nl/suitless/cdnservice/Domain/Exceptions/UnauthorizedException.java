package nl.suitless.cdnservice.Domain.Exceptions;

public class UnauthorizedException extends RuntimeException{
    public UnauthorizedException(String exception) {super(exception);}
}
