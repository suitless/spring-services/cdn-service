package nl.suitless.cdnservice.Domain.Exceptions;

public class MetaDataException extends RuntimeException {
    public MetaDataException(String err){
        super(err);
    }
}
