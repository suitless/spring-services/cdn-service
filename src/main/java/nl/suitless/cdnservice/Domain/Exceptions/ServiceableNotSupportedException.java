package nl.suitless.cdnservice.Domain.Exceptions;

public class ServiceableNotSupportedException extends RuntimeException {
    public ServiceableNotSupportedException(String err) { super(err); }
}
