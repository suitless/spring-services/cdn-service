package nl.suitless.cdnservice.Domain.Exceptions;

public class ServiceableDefaultException extends RuntimeException{
    public ServiceableDefaultException(String err){ super(err); }
}
