package nl.suitless.cdnservice.Domain.Exceptions;

public class ServiceableNotCreatedException extends RuntimeException {
    public ServiceableNotCreatedException(String err){
        super(err);
    }
}
