package nl.suitless.cdnservice.Domain.Exceptions;

public class UnsupportedTokenException extends RuntimeException {
    public UnsupportedTokenException(String exception) { super(exception); }
}
