package nl.suitless.cdnservice.Domain.Exceptions;

public class ServiceableNotFoundException extends RuntimeException {
    public ServiceableNotFoundException(String err){
        super(err);
    }
}
