package nl.suitless.cdnservice.Domain.Static;

import nl.suitless.cdnservice.Domain.Entities.Serviceable;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Base64;

@Service
public class Base64Manager {
    public static String getBase64(Serviceable serviceable){
        try {
            byte[] bytes = serviceable.getData().getBytes(1, (int)serviceable.getData().length());
            return Arrays.toString(Base64.getEncoder().encode(bytes));
        } catch (SQLException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
