package nl.suitless.cdnservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CDNServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(CDNServiceApplication.class, args);
	}
}
