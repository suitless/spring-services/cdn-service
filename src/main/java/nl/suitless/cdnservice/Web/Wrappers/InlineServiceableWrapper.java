package nl.suitless.cdnservice.Web.Wrappers;

public class InlineServiceableWrapper {
    private String id;
    private String tag;
    private String type;
    private Boolean locked;

    public InlineServiceableWrapper(String id, String tag, String type, Boolean locked) {
        this.id = id;
        this.tag = tag;
        this.type = type;
        this.locked = locked;
    }

    public InlineServiceableWrapper() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean isLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }
}
