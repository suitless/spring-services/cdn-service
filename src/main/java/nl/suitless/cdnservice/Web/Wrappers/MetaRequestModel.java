package nl.suitless.cdnservice.Web.Wrappers;

import javax.validation.constraints.NotNull;

public class MetaRequestModel {
    @NotNull
    private Boolean locked;
    @NotNull
    private String owner;
    @NotNull
    private String tag;

    public MetaRequestModel(@NotNull  Boolean locked, @NotNull  String owner, @NotNull String tag) {
        this.locked = locked;
        this.owner = owner;
        this.tag = tag;
    }

    public MetaRequestModel(){

    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
