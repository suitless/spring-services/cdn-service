package nl.suitless.cdnservice.Web.Wrappers;

public class Base64ServiceableWrapper {
    private String id;
    private String tag;
    private String type;
    private String data;
    private Boolean locked;

    public Base64ServiceableWrapper(String id, String tag, String type, String data, Boolean locked) {
        this.id = id;
        this.tag = tag;
        this.type = type;
        this.data = data;
        this.locked = locked;
    }

    public Base64ServiceableWrapper(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }
}
