package nl.suitless.cdnservice.Web.HateosResources;

import nl.suitless.cdnservice.Domain.Entities.ServiceableMeta;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class MetaCDNResources extends RepresentationModel<MetaCDNResources> {
    private List<ServiceableMeta> serviceableMetas;
    private long totalElements;

    public MetaCDNResources() {
    }

    public MetaCDNResources(List<ServiceableMeta> serviceableMetas, long totalElements) {
        this.serviceableMetas = serviceableMetas;
        this.totalElements = totalElements;
    }

    public List<ServiceableMeta> getServiceableMetas() {
        return serviceableMetas;
    }

    public void setServiceableMetas(List<ServiceableMeta> serviceableMetas) {
        this.serviceableMetas = serviceableMetas;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }
}
