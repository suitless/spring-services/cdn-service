package nl.suitless.cdnservice.Web.HateosResources;

import nl.suitless.cdnservice.Domain.Entities.ServiceableMeta;
import org.springframework.hateoas.RepresentationModel;

public class MetaCDNResource extends RepresentationModel<MetaCDNResource> {
    private ServiceableMeta serviceableMeta;

    public MetaCDNResource(ServiceableMeta serviceableMeta) {
        this.serviceableMeta = serviceableMeta;
    }

    public MetaCDNResource() {
    }

    public ServiceableMeta getMetaServiceableWrapper() {
        return serviceableMeta;
    }
}
