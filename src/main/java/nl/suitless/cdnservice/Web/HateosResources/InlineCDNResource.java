package nl.suitless.cdnservice.Web.HateosResources;

import nl.suitless.cdnservice.Web.Wrappers.InlineServiceableWrapper;
import org.springframework.hateoas.RepresentationModel;

public class InlineCDNResource extends RepresentationModel<InlineCDNResource> {
    private InlineServiceableWrapper inlineServiceableWrapper;

    public InlineCDNResource() {
    }

    public InlineCDNResource(InlineServiceableWrapper inlineServiceableWrapper) {
        this.inlineServiceableWrapper = inlineServiceableWrapper;
    }

    public InlineServiceableWrapper getInlineServiceableWrapper() {
        return inlineServiceableWrapper;
    }
}
