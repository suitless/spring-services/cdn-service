package nl.suitless.cdnservice.Web.HateosResources;

import nl.suitless.cdnservice.Web.Wrappers.Base64ServiceableWrapper;
import org.springframework.hateoas.RepresentationModel;

public class Base64CDNResource extends RepresentationModel<Base64CDNResource> {
    private Base64ServiceableWrapper base64ServiceableWrapper;

    public Base64CDNResource(Base64ServiceableWrapper base64ServiceableWrapper) {
        this.base64ServiceableWrapper = base64ServiceableWrapper;
    }

    public Base64CDNResource() {
    }

    public Base64ServiceableWrapper getBase64ServiceableWrapper() {
        return base64ServiceableWrapper;
    }
}
