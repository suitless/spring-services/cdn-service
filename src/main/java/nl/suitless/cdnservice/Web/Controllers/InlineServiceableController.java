package nl.suitless.cdnservice.Web.Controllers;

import nl.suitless.cdnservice.Domain.Entities.AuthorizationConfig;
import nl.suitless.cdnservice.Domain.Entities.Serviceable;
import nl.suitless.cdnservice.Services.Serviceable.IServiceableService;
import nl.suitless.cdnservice.Web.HateosResources.InlineCDNResource;
import nl.suitless.cdnservice.Web.Wrappers.InlineServiceableWrapper;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.sql.SQLException;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Controller
@RequestMapping("")
public class InlineServiceableController {
    private final IServiceableService serviceableService;

    @Autowired
    public InlineServiceableController(IServiceableService serviceableService) {
        this.serviceableService = serviceableService;
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<InlineCDNResource> getServiceableById(@PathVariable("id") String id, HttpServletResponse response) {
        var serviceable = serviceableService.getServiceableById(id);
        sendCdnResource(serviceable, response);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @GetMapping(path = "/{id}/small")
    public ResponseEntity<InlineCDNResource> getSmallImageById(@PathVariable("id") String id, HttpServletResponse response) {
        var serviceable = serviceableService.getImageByIdSmall(id);
        sendCdnResource(serviceable, response);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @GetMapping(path = "/{owner}/{tag}")
    public ResponseEntity<InlineCDNResource> getServiceableByTag(@PathVariable("owner") String owner, @PathVariable("tag") String tag,
                                                                 HttpServletResponse response) {

        var serviceable = serviceableService.getServiceable(tag, owner);
        sendCdnResource(serviceable, response);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @PostMapping(path = "/{owner}")
    public ResponseEntity<InlineCDNResource> uploadServiceable(Principal principal, @PathVariable("owner") String owner, @RequestParam("tag") String tag,
                                                               @RequestParam("fileType") String type, @RequestParam("file") MultipartFile file,
                                                               @RequestParam("locked") boolean locked, HttpServletResponse response) {
        var serviceable = serviceableService.createServiceable(tag, type, file, locked, owner, new AuthorizationConfig(principal));
        return getInlineCDNResourceResponseEntity(principal, response, serviceable);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<InlineCDNResource> updateServiceable(Principal principal, @PathVariable("id") String id, @RequestParam("tag") String tag,
                                                               @RequestParam("fileType") String type, @RequestParam("file") MultipartFile file,
                                                               @RequestParam("locked") boolean locked, @RequestParam("owner") String owner, HttpServletResponse response) {
        var serviceable = serviceableService.updateServiceable(id, tag, type, file, locked, owner, new AuthorizationConfig(principal));
        return getInlineCDNResourceResponseEntity(principal, response, serviceable);
    }

    private ResponseEntity<InlineCDNResource> getInlineCDNResourceResponseEntity(Principal principal, HttpServletResponse response, Serviceable serviceable) {
        var inlineCdnResource = new InlineCDNResource(
                new InlineServiceableWrapper(serviceable.getId(), serviceable.getTag(), serviceable.getType(), serviceable.getLocked()));
        inlineCdnResource.add(linkTo(methodOn(InlineServiceableController.class).getServiceableByTag(
                serviceable.getOwner(), serviceable.getTag(), response)).withSelfRel());
        inlineCdnResource.add(linkTo(methodOn(InlineServiceableController.class).deleteServiceable(principal,
                serviceable.getId())).withRel("Delete"));

        return new ResponseEntity<>(inlineCdnResource, HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteServiceable(Principal principal, @PathVariable("id") String id) {
        serviceableService.deleteServiceable(id, new AuthorizationConfig(principal));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void sendCdnResource(Serviceable serviceable, HttpServletResponse response){
        try {
            response.setHeader("Content-Disposition", "inline;filename=\"" + serviceable.getTag() + "\"");
            OutputStream out = response.getOutputStream();
            response.setContentType(serviceable.getType());
            IOUtils.copy(serviceable.getData().getBinaryStream(), out);
            out.flush();
            out.close();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }
}
