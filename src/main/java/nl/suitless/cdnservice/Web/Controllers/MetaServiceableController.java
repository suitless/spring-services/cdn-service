package nl.suitless.cdnservice.Web.Controllers;

import nl.suitless.cdnservice.Domain.Entities.AuthorizationConfig;
import nl.suitless.cdnservice.Domain.Entities.ServiceableMeta;
import nl.suitless.cdnservice.Domain.Entities.ServiceableType;
import nl.suitless.cdnservice.Domain.Entities.SortingType;
import nl.suitless.cdnservice.Services.Serviceable.IServiceableService;
import nl.suitless.cdnservice.Services.Utils.PageListTranslator;
import nl.suitless.cdnservice.Web.HateosResources.MetaCDNResource;
import nl.suitless.cdnservice.Web.HateosResources.MetaCDNResources;
import nl.suitless.cdnservice.Web.Wrappers.MetaRequestModel;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Controller
@RequestMapping("/meta")
public class MetaServiceableController {

    private final IServiceableService serviceableService;

    public MetaServiceableController(IServiceableService serviceableService) {
        this.serviceableService = serviceableService;
    }

    @GetMapping(path = "/{owner}/all", params = { "type", "page", "size", "sort" })
    public ResponseEntity<MetaCDNResources> getAllMetaData(Principal principal, @PathVariable("owner") String owner, @RequestParam ServiceableType type,
                                                           @RequestParam("page") int page, @RequestParam("size") int size, @RequestParam("sort")SortingType sortType) {
        Page<ServiceableMeta> result = serviceableService.getAllMetaData(type, sortType, owner, page, size, new AuthorizationConfig(principal));
        var metaCDNResources = new MetaCDNResources(PageListTranslator.PageMetaToList(result), result.getTotalElements());
        metaCDNResources.add(linkTo(methodOn(MetaServiceableController.class).getAllMetaData(principal, owner, type, page, size, sortType)).withSelfRel());

        return new ResponseEntity<>(metaCDNResources, HttpStatus.OK);
    }

    @GetMapping(path = "/{owner}/search", params = { "type", "tag", "page", "size" })
    public ResponseEntity<MetaCDNResources> searchMetaDataByTag(Principal principal, @PathVariable("owner") String owner,
                                                                @RequestParam("type") ServiceableType type, @RequestParam("tag") String tag,
                                                                @RequestParam("page") int page, @RequestParam("size") int size) {
        Page<ServiceableMeta> result = serviceableService.searchMetaDataByTag(type, owner, tag, page, size, new AuthorizationConfig(principal));
        var metaCDNResources = new MetaCDNResources(PageListTranslator.PageMetaToList(result), result.getTotalElements());
        metaCDNResources.add(linkTo(methodOn(MetaServiceableController.class)
                .searchMetaDataByTag(principal, owner, type, tag, page, size)).withSelfRel());

        return new ResponseEntity<>(metaCDNResources, HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<MetaCDNResource> getServiceableById(@PathVariable("id") String id) {
        MetaCDNResource metaCDNResource = new MetaCDNResource(serviceableService.getMetaData(id));
        metaCDNResource.add(linkTo(methodOn(MetaServiceableController.class).getServiceableById(id)).withSelfRel());

        return new ResponseEntity<>(metaCDNResource, HttpStatus.OK);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<MetaCDNResource> updateMetaDataOfServiceable(Principal principal, @PathVariable("id") String id,
                                                                       @RequestBody MetaRequestModel requestModel) {
         MetaCDNResource metaResource = new MetaCDNResource(
                 serviceableService.updateMetaData(id, requestModel.getTag(), requestModel.getLocked(),
                         requestModel.getOwner(), new AuthorizationConfig(principal)));
         metaResource.add(linkTo(methodOn(MetaServiceableController.class)
                 .updateMetaDataOfServiceable(principal, id, requestModel)).withSelfRel());

         return new ResponseEntity<>(metaResource, HttpStatus.OK);
    }
}
