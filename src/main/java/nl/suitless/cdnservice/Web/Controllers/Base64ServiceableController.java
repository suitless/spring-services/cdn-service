package nl.suitless.cdnservice.Web.Controllers;

import nl.suitless.cdnservice.Domain.Entities.AuthorizationConfig;
import nl.suitless.cdnservice.Domain.Entities.Serviceable;
import nl.suitless.cdnservice.Services.Serviceable.IServiceableService;
import nl.suitless.cdnservice.Web.HateosResources.Base64CDNResource;
import nl.suitless.cdnservice.Web.HateosResources.InlineCDNResource;
import nl.suitless.cdnservice.Web.Wrappers.Base64ServiceableWrapper;
import nl.suitless.cdnservice.Web.Wrappers.InlineServiceableWrapper;
import nl.suitless.cdnservice.Domain.Static.Base64Manager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

import java.security.Principal;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Controller
@RequestMapping("/base64")
public class Base64ServiceableController {

    private final IServiceableService serviceableService;

    public Base64ServiceableController(IServiceableService serviceableService) {
        this.serviceableService = serviceableService;
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Base64CDNResource> getServiceableById(Principal principal, @PathVariable("id") String id,
                                                                HttpServletResponse response) {

        var serviceable = serviceableService.getServiceableById(id);

        return getBase64CDNResourceResponseEntity(principal, response, serviceable);
    }

    @GetMapping(path = "/{owner}/{tag}")
    public ResponseEntity<Base64CDNResource> getServiceableByTag(Principal principal, @PathVariable("owner") String owner,
                                                                 @PathVariable("tag") String tag, HttpServletResponse response) {

        Serviceable serviceable = serviceableService.getServiceable(tag, owner);
        return getBase64CDNResourceResponseEntity(principal, response, serviceable);
    }

    private ResponseEntity<Base64CDNResource> getBase64CDNResourceResponseEntity(Principal principal,
                                                                                 HttpServletResponse response, Serviceable serviceable) {
        Base64CDNResource base64CDNResource = new Base64CDNResource(
                new Base64ServiceableWrapper(serviceable.getId(), serviceable.getTag(), serviceable.getType(),
                        Base64Manager.getBase64(serviceable), serviceable.getLocked()));

        base64CDNResource.add(linkTo(methodOn(Base64ServiceableController.class).getServiceableByTag(principal,
                serviceable.getOwner(), serviceable.getTag(), response)).withSelfRel());
        base64CDNResource.add(linkTo(methodOn(InlineServiceableController.class).deleteServiceable(principal,
                serviceable.getId())).withRel("Delete"));

        return new ResponseEntity<>(base64CDNResource, HttpStatus.OK);
    }

    @PostMapping(path = "/{owner}")
    public ResponseEntity<InlineCDNResource> uploadServiceable(Principal principal, @PathVariable("owner") String owner, @RequestParam("tag") String tag,
                                                               @RequestParam("fileType") String type, @RequestParam("file") String file,
                                                               @RequestParam("locked") boolean locked, HttpServletResponse response) {

        var serviceable = serviceableService.createServiceable(tag, type, file, locked, owner, new AuthorizationConfig(principal));
        return getInlineCDNResourceResponseEntity(principal, response, serviceable);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<InlineCDNResource> updateServiceable(Principal principal, @PathVariable("id") String id, @RequestParam("tag") String tag,
                                                               @RequestParam("fileType") String type, @RequestParam("file") String file,
                                                               @RequestParam("locked") boolean locked, @RequestParam("owner") String owner, HttpServletResponse response) {

        var serviceable = serviceableService.updateServiceable(id, tag, type, file, locked, owner, new AuthorizationConfig(principal));
        return getInlineCDNResourceResponseEntity(principal, response, serviceable);
    }

    private ResponseEntity<InlineCDNResource> getInlineCDNResourceResponseEntity(Principal principal,  HttpServletResponse response, Serviceable serviceable) {
        InlineCDNResource inlineCdnResource = new InlineCDNResource(
                new InlineServiceableWrapper(serviceable.getId(), serviceable.getTag(), serviceable.getType(), serviceable.getLocked()));

        inlineCdnResource.add(linkTo(methodOn(Base64ServiceableController.class).getServiceableByTag(principal,
                serviceable.getOwner(), serviceable.getTag(), response)).withSelfRel());
        inlineCdnResource.add(linkTo(methodOn(InlineServiceableController.class).deleteServiceable(principal,
                serviceable.getId())).withRel("Delete"));

        return new ResponseEntity<>(inlineCdnResource, HttpStatus.OK);
    }
}
