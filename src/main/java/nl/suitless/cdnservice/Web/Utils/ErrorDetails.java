package nl.suitless.cdnservice.Web.Utils;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Custom wrapper used for sending standardized error details back to the user.
 * @author Martijn Dormans NU 19, copied from Nick (bold guy) van der burgt
 * @version 1.0
 * @since 23-8-2019
 */
public class ErrorDetails {
    @JsonProperty
    private Date timestamp;
    @JsonProperty
    private String message;
    @JsonProperty
    private String details;

    public ErrorDetails() {
    }

    public ErrorDetails(Date timestamp, String message, String details) {
        super();
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
    }
}