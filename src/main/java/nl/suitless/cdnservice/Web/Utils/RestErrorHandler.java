package nl.suitless.cdnservice.Web.Utils;

import nl.suitless.cdnservice.Domain.Exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class RestErrorHandler {
    @ExceptionHandler(value = MetaDataException.class)
    public final ResponseEntity<ErrorDetails> handleMetaDataException(MetaDataException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = ServiceableDefaultException.class)
    public final ResponseEntity<ErrorDetails> handleServiceableDefaultException(ServiceableDefaultException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.LOCKED);
    }

    @ExceptionHandler(value = ServiceableNotCreatedException.class)
    public final ResponseEntity<ErrorDetails> handleServiceableNotCreatedException(ServiceableNotCreatedException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = ServiceableNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleServiceableNotFoundException(ServiceableNotFoundException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ServiceableNotSupportedException.class)
    public final ResponseEntity<ErrorDetails> handleServiceableNotSupportedException(ServiceableNotFoundException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = UnauthorizedException.class)
    public final ResponseEntity<ErrorDetails> handleUnauthorizedException(UnauthorizedException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = UnsupportedTokenException.class)
    public final ResponseEntity<ErrorDetails> handleUnsupportedTokenException(UnsupportedTokenException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
