package nl.suitless.cdnservice;

import nl.suitless.cdnservice.Config.ImageResize.ImageResizeConfig;
import nl.suitless.cdnservice.Data.IServiceableRepository;
import nl.suitless.cdnservice.Domain.Entities.*;
import nl.suitless.cdnservice.Domain.Exceptions.ServiceableDefaultException;
import nl.suitless.cdnservice.Domain.Exceptions.ServiceableNotFoundException;
import nl.suitless.cdnservice.Domain.Exceptions.ServiceableNotCreatedException;
import nl.suitless.cdnservice.Domain.Exceptions.ServiceableNotSupportedException;
import nl.suitless.cdnservice.Services.Authorization.IAuthorizationService;
import nl.suitless.cdnservice.Services.Serviceable.ServiceableService;

import java.io.*;

import org.aspectj.util.FileUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.*;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ServiceableServiceUnitTests {
	private Serviceable textServiceable;
	private Serviceable defaultTextServiceable;
	private Serviceable imageServiceable;
	private ServiceableMeta serviceableMeta;
	private Page<ServiceableMeta> serviceableMetaPage;

	private AuthorizationConfig authConfig;
	private Collection<GrantedAuthority> authorities;
	private String jwt;

	@Mock
	private IServiceableRepository serviceableRepository;
	@Mock
	private IAuthorizationService authorizationService;
	@Mock
	private ImageResizeConfig imageResizeConfig;

	@InjectMocks
	private ServiceableService serviceableService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		String tag = "TestService";
		String type = "text/plain";
		Blob textData;

		String defaultTag = "DefaultService";

		String imageTag = "TestImage";
		String imageType = "image/png";
		Blob imageData;

		try {
			var serviceableMetaList = new ArrayList<ServiceableMeta>();

			textData = new SerialBlob("TestData".getBytes());
			textServiceable = new Serviceable(textData, tag, type, 10, false, "Suitless");
			serviceableMetaList.add(new ServiceableMeta("1-2-3-4", textServiceable.getTag(), textServiceable.getType(),
					textServiceable.getSize(), textServiceable.getLocked()));

			defaultTextServiceable = new Serviceable(textData, defaultTag, type, 10, true, "Suitless");

			imageData = new SerialBlob(getTestImage());
			imageServiceable = new Serviceable(imageData, imageTag, imageType, 10, false, "Suitless");
			serviceableMetaList.add(new ServiceableMeta(imageServiceable.getId(), imageServiceable.getTag(), imageServiceable.getType(),
					imageServiceable.getSize(), imageServiceable.getLocked()));

			serviceableMetaPage = new PageImpl<>(serviceableMetaList);

			serviceableMeta = new ServiceableMeta("1-2-3-4", textServiceable.getTag(), textServiceable.getType(),
					textServiceable.getSize(), textServiceable.getLocked());
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}

		authorities = new ArrayList<>();
		((ArrayList<GrantedAuthority>) authorities).add(0, new SimpleGrantedAuthority("editor"));
		jwt = "jwt";
		authConfig = mock(AuthorizationConfig.class, RETURNS_DEEP_STUBS);
	}

	private byte[] getTestImage() throws IOException {
		File file = new File("src/test/java/nl/suitless/cdnservice/testImage.jpg");
		return FileUtil.readAsByteArray(file);
	}

	@Test
	public void getAllContentTypesValid() {
		List<String> types = serviceableService.getAllServiceableTypes();

		verify(serviceableRepository, times(1)).findAllContentTypes();
		Assert.assertEquals(0, types.size());
	}

	@Test
	public void getServiceableValid() {
		when(serviceableRepository.findByTagAndOwner(anyString(), anyString())).thenReturn(Optional.of(textServiceable));

		var foundServiceable = serviceableService.getServiceable("TestService", "Suitless");

		verify(serviceableRepository, times(1)).findByTagAndOwner("TestService", "Suitless");
		Assert.assertEquals(textServiceable, foundServiceable);
	}

	@Test(expected = ServiceableNotFoundException.class)
	public void findByTagDoesNotExist() {
		when(serviceableRepository.findByTagAndOwner(anyString(), anyString())).thenReturn(Optional.empty());

		serviceableService.getServiceable("NicksHaarlijn", "PietJanBv");
	}

	@Test
	public void getImageByIdSmallValid() throws SQLException {
		var length = imageServiceable.getData().length();
		when(serviceableRepository.findDistinctFirstById(anyString())).thenReturn(Optional.of(imageServiceable));
		when(imageResizeConfig.getHeight()).thenReturn(200);
		when(imageResizeConfig.getWidth()).thenReturn(200);

		var foundServiceable = serviceableService.getImageByIdSmall("1-2-3-4");

		verify(serviceableRepository, times(1)).findDistinctFirstById("1-2-3-4");
		Assert.assertNotEquals(length, foundServiceable.getData().length());
	}

	@Test(expected = ServiceableNotSupportedException.class)
	public void getImageByIdSmallNotSupported() {
		when(serviceableRepository.findDistinctFirstById(anyString())).thenReturn(Optional.of(textServiceable));

		serviceableService.getImageByIdSmall("1-2-3-4");
	}

	@Test(expected = ServiceableNotFoundException.class)
	public void getImageByIdNotFound() {
		serviceableService.getImageByIdSmall(imageServiceable.getId());
	}

	@Test
	public void getServiceableByIdValid() {
		when(serviceableRepository.findDistinctFirstById(anyString())).thenReturn(Optional.of(textServiceable));

		Serviceable foundServiceable = serviceableService.getServiceableById("1-2-3-4");

		verify(serviceableRepository, times(1)).findDistinctFirstById("1-2-3-4");
		Assert.assertEquals(textServiceable.getTag(), foundServiceable.getTag());
	}

	@Test(expected = ServiceableNotFoundException.class)
	public void getServiceableByIdNotFound() {
		serviceableService.getServiceableById("1-2-3-4");
	}

	@Test
	public void createServiceableWithMultiPartFileValid() {
		when(serviceableRepository.save(any(Serviceable.class))).thenReturn(textServiceable);
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		var createdServiceable = serviceableService.createServiceable("myContent", "TestService", new MockMultipartFile(), false, "Suitless", authConfig);

		verify(serviceableRepository, times(1)).save(any(Serviceable.class));
		Assert.assertEquals(textServiceable, createdServiceable);
	}

	@Test(expected = ServiceableNotCreatedException.class)
	public void createServiceableWithMultiPartFileNotCreated() {
		serviceableService.createServiceable("myContent", "TestService", new MockFalseMultipartFile(), false, "Suitless", authConfig);
	}

	@Test
	public void createServiceableValid() {
		when(serviceableRepository.save(any(Serviceable.class))).thenReturn(textServiceable);
		when(serviceableRepository.findByTagAndOwner("TestService", "Suitless")).thenReturn(Optional.of(textServiceable));
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		var createdServiceable = serviceableService.createServiceable("TestService", "text/plain", "data", false, "Suitless", authConfig);

		verify(serviceableRepository, times(1)).save(any(Serviceable.class));
		Assert.assertEquals(createdServiceable, textServiceable);
	}

	@Test(expected = ServiceableNotCreatedException.class)
	public void createServiceableNotCreated() {
		serviceableService.createServiceable("TestService", "text/plain", "@��", false, "Suitless", authConfig);
	}

	@Test
	public void updateServiceableWithMultiPartFileValid() {
		String tag = "TestService";
		String type = "text/plain";
		Blob data;
		Serviceable serviceable1 = null;
		try {
			data = new SerialBlob("NewTestData".getBytes());
			serviceable1 = new Serviceable(data, tag, type, data.length(), false, "Suitless");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		when(serviceableRepository.save(any(Serviceable.class))).thenReturn(serviceable1);
		when(serviceableRepository.findDistinctFirstById(anyString())).thenReturn(Optional.of(textServiceable));
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		var updatedServiceable = serviceableService.updateServiceable("1-2-3-4", "TestService", "text/plain", new MockMultipartFile(), false, "Suitless", authConfig);

		verify(serviceableRepository, times(1)).save(any(Serviceable.class));
		Assert.assertNotEquals(updatedServiceable, textServiceable);
	}

	@Test(expected = ServiceableNotCreatedException.class)
	public void updateServiceableWithMultiPartFileNotCreated() {
		serviceableService.updateServiceable("1-2-3-4", "myContent", "TestService", new MockFalseMultipartFile(), false, "Suitless", authConfig);
	}

	@Test
	public void updateServiceableValid() {
		String tag = "TestService";
		String type = "text/plain";
		Blob data;
		Serviceable serviceable1 = null;
		try {
			data = new SerialBlob("NewTestData".getBytes());
			serviceable1 = new Serviceable(data, tag, type, data.length(), false, "Suitless");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		when(serviceableRepository.save(any(Serviceable.class))).thenReturn(serviceable1);
		when(serviceableRepository.findDistinctFirstById(anyString())).thenReturn(Optional.of(textServiceable));
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		var updatedServiceable = serviceableService.updateServiceable("1-2-3-4", "TestService", "text/plain", "TestData", false, "Suitless", authConfig);

		verify(serviceableRepository, times(1)).save(any(Serviceable.class));
		Assert.assertNotEquals(updatedServiceable, textServiceable);
	}

	@Test(expected = ServiceableNotCreatedException.class)
	public void updateServiceableNotCreated() {
		serviceableService.updateServiceable("1-2-3-4", "TestService", "text/plain", "@��", false, "Suitless", authConfig);
	}

	@Test
	public void deleteServiceableValid() {
		when(serviceableRepository.findDistinctFirstById(anyString())).thenReturn(Optional.of(textServiceable));
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		serviceableService.deleteServiceable("1-2-3-4", authConfig);

		verify(serviceableRepository, times(1)).delete(any(Serviceable.class));
	}

	@Test(expected = ServiceableNotFoundException.class)
	public void deleteServiceableNotFound() {
		when(serviceableRepository.findDistinctFirstById(anyString())).thenReturn(Optional.empty());
		when(authConfig.getAuthorities()).thenReturn(authorities);

		serviceableService.deleteServiceable("PietjanBv", authConfig);
	}

	@Test(expected = ServiceableDefaultException.class)
	public void deleteModuleIsDefault() {
		when(serviceableRepository.findDistinctFirstById(anyString())).thenReturn(Optional.of(defaultTextServiceable));
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		serviceableService.deleteServiceable("PietjanBv", authConfig);
	}

	@Test
	public void getAllMetaDataValidSortByTag() {
		when(serviceableRepository.getAllByDataIsNotNullAndOwnerAndTypeContainingOrderByTag(anyString(), anyString(), any())).thenReturn(serviceableMetaPage);
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		var foundMetaData = serviceableService.getAllMetaData(ServiceableType.ALL,SortingType.TAG,"Suitless",  1, 1, authConfig);

		verify(serviceableRepository, times(1)).getAllByDataIsNotNullAndOwnerAndTypeContainingOrderByTag(anyString(), anyString(), any());
		Assert.assertEquals("1-2-3-4", foundMetaData.getContent().get(0).getId());
	}

	@Test
	public void getAllMetaDataValidSortBySize() {
		when(serviceableRepository.getAllByDataIsNotNullAndOwnerAndTypeContainingOrderBySize(anyString(), anyString(), any())).thenReturn(serviceableMetaPage);
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		var foundMetaData = serviceableService.getAllMetaData(ServiceableType.ALL,SortingType.SIZE,"Suitless",  1, 1, authConfig);

		verify(serviceableRepository, times(1)).getAllByDataIsNotNullAndOwnerAndTypeContainingOrderBySize(anyString(), anyString(), any());
		Assert.assertEquals("1-2-3-4", foundMetaData.getContent().get(0).getId());
	}

	@Test
	public void getMetaDataValid() {
		when(serviceableRepository.findFirstById(anyString())).thenReturn(Optional.of(serviceableMeta));

		var foundMetaData = serviceableService.getMetaData("furry");

		verify(serviceableRepository, times(1)).findFirstById(anyString());
		Assert.assertEquals(serviceableMeta, foundMetaData);
	}

	@Test(expected = ServiceableNotFoundException.class)
	public void getMetaDataNotFound() {
		when(serviceableRepository.findFirstById(anyString())).thenReturn(Optional.empty());

		serviceableService.getMetaData("furry");
	}

	@Test
	public void updateMetaDataValid() {
		when(serviceableRepository.findDistinctFirstById(anyString())).thenReturn(Optional.of(textServiceable));
		when(authConfig.getAuthorities()).thenReturn(authorities);

		ServiceableMeta foundServiceable = serviceableService.updateMetaData("1-2-3-4","TestService", true, "Suitless", authConfig);
		verify(serviceableRepository, times(1)).findByTagAndOwner(anyString(), anyString());
		Assert.assertTrue(foundServiceable.isLocked());
	}
}

